package com.ms.sam.music_player;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

   private MediaPlayer mp;
   private ListView listView;
   private String songs[];
   private Button play,stop,next;
    Boolean pause=false;
   private int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        play=findViewById(R.id.playandpause);
        stop=findViewById(R.id.stop);
        next=findViewById(R.id.next);

        listView=findViewById(R.id.listview);

        songs=getResources().getStringArray(R.array.song);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>
                (this,android.R.layout.simple_list_item_1,songs);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                if (i==0)
                {
                    status=i;

                    if (mp!= null)
                    {
                        mp.stop();
                    }
                    mp = MediaPlayer.create(MainActivity.this, R.raw.suno_sangemarmar);
                    mp.start();
                    play.setText("pause");
                    pause=true;
                   // status=status+1;

                }

                if (i==1)
                {
                   status=i;
                    if (mp!= null)
                    {
                        mp.stop();
                    }
                    mp = MediaPlayer.create(MainActivity.this, R.raw.baatein_ye_kabhi_na);

                    mp.start();
                    play.setText("pause");
                    pause=true;
                   // status=status+1;

                }

                if (i==2)
                {
                    status=i;
                    if (mp!= null)
                    {
                        mp.stop();
                    }
                    mp = MediaPlayer.create(MainActivity.this, R.raw.baatein_kuch_ankahee_si);

                    mp.start();
                    play.setText("pause");
                    pause=true;
                }

                if (i==3)
                {
                    status=i;
                    if (mp!= null)
                    {
                        mp.stop();
                    }
                    mp = MediaPlayer.create(MainActivity.this, R.raw.kuch_to_hai);

                    mp.start();
                    play.setText("pause");
                    pause=true;
                }

                if (i==4)
                {
                    status=i;
                    if (mp!= null)
                    {
                        mp.stop();
                    }
                    mp = MediaPlayer.create(MainActivity.this, R.raw.luka_chuppi);

                    mp.start();
                    play.setText("pause");
                    pause=true;
                }




            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (mp==null)
                {
          Toast.makeText(MainActivity.this, "No song is Playing", Toast.LENGTH_SHORT).show();
                }
                else {
                    mp.stop();
                   // play.setText("play");
                }

            }
        });

        play.setText("play");

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (pause == false)
                {
                    if (mp == null)
                    {
                 //   Toast.makeText(MainActivity.this, "Please select song", Toast.LENGTH_SHORT).show();
                        mp = MediaPlayer.create(MainActivity.this, R.raw.suno_sangemarmar);
                        play.setText("Pause");
                        pause = true;
                        mp.start();


                    } else
                        {
                            mp = MediaPlayer.create(MainActivity.this, R.raw.suno_sangemarmar);
                        play.setText("Pause");
                        pause = true;
                        mp.start();

                    }
                }

                 else {

                        play.setText("Play");
                        pause = false;
                        mp.pause();
                    }

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                status=status+1;


           //     Toast.makeText(MainActivity.this, ""+status, Toast.LENGTH_SHORT).show();

                if (status==5)
                {
                    if (mp!= null)
                    {
                        mp.stop();
                    }


                    mp = MediaPlayer.create(MainActivity.this, R.raw.suno_sangemarmar);

                    mp.start();
                    status=0;
                    // status=status+1;


                }
                else

                if (status==1)
                {


                    if (mp!= null)
                    {
                        mp.stop();
                    }
                    mp = MediaPlayer.create(MainActivity.this, R.raw.baatein_ye_kabhi_na);

                    mp.start();
                  // status=status+1;


                }
                else

                if (status==2)
                {

                    if (mp!= null)
                    {
                        mp.stop();
                    }
                    mp = MediaPlayer.create(MainActivity.this, R.raw.baatein_kuch_ankahee_si);

                    mp.start();
                    //  status=status-2;


                }

                else

                if (status==3)
                {

                    if (mp!= null)
                    {
                        mp.stop();
                    }
                    mp = MediaPlayer.create(MainActivity.this, R.raw.kuch_to_hai);

                    mp.start();
                    //  status=status-2;


                }

                else

                if (status==4)
                {

                    if (mp!= null)
                    {
                        mp.stop();
                    }
                    mp = MediaPlayer.create(MainActivity.this, R.raw.luka_chuppi);

                    mp.start();
                    //  status=status-2;


                }





            }
        });


    }
}
